#include "Strip.h"
#include <QDebug>
#include <QPainter>
#include <QScxmlStateMachine>
#include <QScxmlEvent>
#include <QGraphicsSceneMouseEvent>
#include <QTimeLine>
#include <QGraphicsItemAnimation>
#include "StripGesturesCtrl.h"

#define MOVE_GESTURE_SIZE 20 // size in px for generating a gesture by moving strip
#define STRIP_RECT QRectF(0, 0, 300, 200) // Position and size of my object

Strip::Strip()
{
    // for catching mouse event
    setFlags(QGraphicsItem::ItemIsSelectable);

    _stripGesturesCtrl = new StripGesturesCtrl(this);
}

// draw this strip
void Strip::paint(QPainter *p, const QStyleOptionGraphicsItem *item, QWidget *widget)
{
    Q_UNUSED(item);
    Q_UNUSED(widget);

    if(_stripGesturesCtrl->isVisible())
    {
        p->drawRect(STRIP_RECT);
        p->drawText(STRIP_RECT, "Strip");
        p->drawText(STRIP_RECT, Qt::AlignCenter, QStringLiteral("State: %1").arg(_statusString));
    }
}

QRectF Strip::boundingRect() const
{
    return STRIP_RECT;
}

void Strip::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    // qDebug() << "Strip: mouseMoveEvent " << event->button();

    if(!_hasGesture)
    {
        if(_cursorClickPos.x() < event->pos().x() - MOVE_GESTURE_SIZE) // moved 20px to the right
        {
            _stripGesturesCtrl->submitEvent("right");
            _hasGesture = true;
        }
        else if(_cursorClickPos.x() > event->pos().x() + MOVE_GESTURE_SIZE) // moved 20px to the left
        {
            _stripGesturesCtrl->submitEvent("left");
            _hasGesture = true;
        }
    }

    // update();
    QGraphicsObject::mouseMoveEvent(event);
}

void Strip::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    //qDebug() << "Strip: mousePressEvent";

    if(event->button() == Qt::LeftButton)
    {
        _cursorClickPos = event->pos();
        _hasGesture = false;
    }

    QGraphicsObject::mousePressEvent(event);
}

void Strip::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    //qDebug() << "Strip: mousePressEvent";

    if(event->button() == Qt::LeftButton)
    {
        _cursorClickPos = QPointF();
        _hasGesture = false;
    }

    QGraphicsObject::mousePressEvent(event);
}


