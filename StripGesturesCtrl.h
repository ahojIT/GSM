#ifndef STRIPGESTURESCTRL_H
#define STRIPGESTURESCTRL_H

#include <QObject>
#include "Strip.h"

class QScxmlEvent;
class QScxmlStateMachine;
class QTimeLine;
class QGraphicsItemAnimation;

class StripGesturesCtrl : public QObject
{
    Q_OBJECT
public:
    explicit StripGesturesCtrl(Strip *parent = nullptr);
    void submitEvent(const QString &eventName);
    QString activeStateName() {return _stateName;}
    bool isVisible() {return _scale>0;}

signals:

private slots:
    void entry(const QScxmlEvent &event);

private:
    void stateAnimation(QPointF pos, qreal scale);

private:
    Strip *_strip;
    QScxmlStateMachine *_stateMachine;
    QTimeLine *_timer;
    QGraphicsItemAnimation *_animation;
    qreal _scale = 0;
    QString _stateName;
};

#endif // STRIPGESTURESCTRL_H
