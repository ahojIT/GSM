#ifndef STRIP_H
#define STRIP_H

#include <QObject>
#include <QGraphicsObject>

class QGraphicsSceneMouseEvent;
class StripGesturesCtrl;

class Strip : public QGraphicsObject
{
    Q_OBJECT

public:
    Strip();
    QRectF boundingRect() const;

protected:
    void paint(QPainter *p, const QStyleOptionGraphicsItem *item, QWidget *widget);

    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    QString _statusString = "Tap on me";
    QPointF _cursorClickPos = QPointF();
    bool _hasGesture = false;
    StripGesturesCtrl *_stripGesturesCtrl;
};

#endif // STRIP_H
