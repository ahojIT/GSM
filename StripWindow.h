#ifndef STRIPWINDOW_H
#define STRIPWINDOW_H

#include <QMainWindow>
#include <QGraphicsSceneMouseEvent>
#include "Strip.h"

namespace Ui {
class StripWindow;
}

class StripWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StripWindow(QWidget *parent = 0);
    ~StripWindow();

private:
    Ui::StripWindow *ui;
    QGraphicsScene *scene;
    Strip *strip;
};

#endif // STRIPWINDOW_H
