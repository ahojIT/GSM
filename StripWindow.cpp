#include "StripWindow.h"
#include "ui_StripWindow.h"
#include <QDebug>
#include <QGraphicsScene>

StripWindow::StripWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::StripWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setMouseTracking(true);

    strip = new Strip();
    scene->addItem(strip);

    scene->addRect(strip->boundingRect().adjusted(-20, -20, 20, 20), QPen(Qt::blue));
}

StripWindow::~StripWindow()
{
    ui->graphicsView->scene()->clear();
    delete ui->graphicsView->scene();
    delete ui;
}
