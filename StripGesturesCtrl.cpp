#include "StripGesturesCtrl.h"
#include <QDebug>
#include <QPainter>
#include <QScxmlStateMachine>
#include <QScxmlEvent>
#include <QGraphicsSceneMouseEvent>
#include <QTimeLine>
#include <QGraphicsItemAnimation>

StripGesturesCtrl::StripGesturesCtrl(Strip *parent) : QObject(parent)
{
    _strip = parent;

    _strip->setPos(_strip->boundingRect().center());

    // initialize state machine
    _stateMachine = QScxmlStateMachine::fromFile(QStringLiteral(":StripGesture.scxml"));
    _stateMachine->setParent(this);
    _stateMachine->connectToEvent("entry", this, &StripGesturesCtrl::entry);
    _stateMachine->start();

    // initialize animations
    _timer = new QTimeLine(500); // 0.5 sec. for animated transitions
    _animation = new QGraphicsItemAnimation;
    _animation->setItem(_strip);
    _animation->setTimeLine(_timer);
}

void StripGesturesCtrl::submitEvent(const QString &eventName)
{
    _stateMachine->submitEvent(eventName);
}

// entry event from scxml states
void StripGesturesCtrl::entry(const QScxmlEvent &event)
{
    Q_UNUSED(event)

    if(_stateMachine->activeStateNames().size() > 0)
    {
        qreal scale = 1;
        QPointF toPos(20, 0);

        _stateName = _stateMachine->activeStateNames().first();

        // set animation properties by current state
        if(_stateName == "normal") {
            scale = 1;
            toPos = QPointF(0, 0);
        } else if(_stateName == "selected") {
            scale = 1;
            toPos = QPointF(20, 0);
        } else if(_stateName == "closed") {
            scale = 0.1;
            toPos = QPointF(_strip->boundingRect().width(), _strip->boundingRect().height());
        } else if(_stateName == "smaller") {
            scale = 0.5;
            toPos = QPointF(0, _strip->boundingRect().height() * (1 - scale));
        }

        stateAnimation(toPos, scale);
    }

    _strip->update();
}

// start an animation
void StripGesturesCtrl::stateAnimation(QPointF pos, qreal scale)
{
    QPointF currPos = _strip->pos();
    _animation->setPosAt(0, currPos);
    _animation->setPosAt(1, pos);
    _animation->setScaleAt(0, _scale, _scale);
    _animation->setScaleAt(1, scale, scale);
    _timer->start();
    _scale = scale;
}
